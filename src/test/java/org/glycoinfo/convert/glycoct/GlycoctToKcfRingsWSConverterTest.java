/**
 * 
 */
package org.glycoinfo.convert.glycoct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glycoinfo.convert.ConvertTest;
import org.glycoinfo.convert.GlyConvert;
import org.glycoinfo.convert.glycoct.GlycoctToKcfWSConverter;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author aoki
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = GlycoctToKcfRingsWSConverterTest.class)
@Configuration
public class GlycoctToKcfRingsWSConverterTest extends ConvertTest {

	Log logger = LogFactory.getLog(GlycoctToKcfRingsWSConverterTest.class);
	
	@Bean
	public GlyConvert getConverter() {
		return new GlycoctToKcfWSConverter();
	}
}
