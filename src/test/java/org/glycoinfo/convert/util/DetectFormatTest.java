package org.glycoinfo.convert.util;


import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glycoinfo.convert.GlyConvert;
import org.glycoinfo.convert.util.DetectFormat;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = DetectFormatTest.class)
public class DetectFormatTest {
	private static Log logger = LogFactory.getLog(DetectFormatTest.class);

	public void detect() {
		logger.debug("no sequence");
	}

	@Test
	public void split() {

		String sequence = "RES\n" + 
	"1b:a-dgal-HEX-1:5\n" + 
				"2s:n-acetyl\n" + 
				"3b:b-dgal-HEX-1:5\n" + 
				"LIN\n" + 
				"1:1d(2+1)2n\n" + 
				"2:1o(3+1)3d";
		
		String sequence2 = "RES\n"
				+ "1b:x-dgal-HEX-1:5\n"
				+ "2s:n-acetyl\n"
				+ "3b:b-dgal-HEX-1:5\n"
				+ "4b:a-dgal-HEX-1:5\n"
				+ "5s:n-acetyl\n"
				+ "6b:b-dgal-HEX-1:5\n"
				+ "7b:b-dglc-HEX-1:5\n"
				+ "8s:n-acetyl\n"
				+ "9s:sulfate\n"
				+ "LIN\n"
				+ "1:1d(2+1)2n\n"
				+ "2:1o(3+1)3d\n"
				+ "3:3o(4+1)4d\n"
				+ "4:4d(2+1)5n\n"
				+ "5:4o(3+1)6d\n"
				+ "6:1o(6+1)7d\n"
				+ "7:7d(2+1)8n\n"
				+ "8:7o(6+1)9n";
			
		
		logger.debug("sequence:>" + sequence + "<");
		logger.debug("sequence2:>" + sequence2 + "<");
		String input = sequence+"\n"+sequence2;
		
		List<String> output = DetectFormat.split(input);
		for (Iterator iterator = output.iterator(); iterator.hasNext();) {
			String string = (String) iterator.next();
			logger.debug(">" + string.trim() + "<");
		}
		
		Assert.assertEquals(output.size(), 2);
	}
	
	@Test
	public void splitloop() {

		String sequence = "RES\n"
				+ "1b:a-dgal-HEX-1:5\n"
				+ "2s:N-acetyl\n"
				+ "3b:b-dgal-HEX-1:5\n"
				+ "4r:r1\n"
				+ "5b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d\n"
				+ "6s:n-acetyl\n"
				+ "7r:r2\n"
				+ "8b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d\n"
				+ "9s:n-acetyl\n"
				+ "LIN\n"
				+ "1:1d(2+1)2n\n"
				+ "2:1o(3+1)3d\n"
				+ "3:3o(3+1)4n\n"
				+ "4:4n(3+2)5d\n"
				+ "5:5d(5+1)6n\n"
				+ "6:1o(6+1)7n\n"
				+ "7:7n(3+2)8d\n"
				+ "8:8d(5+1)9n\n"
				+ "REP\n"
				+ "REP1:13o(3+1)10d=-1--1\n"
				+ "RES\n"
				+ "10b:b-dglc-HEX-1:5\n"
				+ "11s:n-acetyl\n"
				+ "12b:a-lgal-HEX-1:5|6:d\n"
				+ "13b:b-dgal-HEX-1:5\n"
				+ "14s:sulfate\n"
				+ "LIN\n"
				+ "9:10d(2+1)11n\n"
				+ "10:10o(3+1)12d\n"
				+ "11:10o(4+1)13d\n"
				+ "12:10o(6+1)14n\n"
				+ "REP2:18o(3+1)15d=-1--1\n"
				+ "RES\n"
				+ "15b:b-dgal-HEX-1:5\n"
				+ "16s:n-acetyl\n"
				+ "17b:a-lgal-HEX-1:5|6:d\n"
				+ "18b:b-dgal-HEX-1:5\n"
				+ "19s:sulfate\n"
				+ "LIN\n"
				+ "13:15d(2+1)16n\n"
				+ "14:15o(3+1)17d\n"
				+ "15:15o(4+1)18d\n"
				+ "16:15o(6+1)19n";
		
		logger.debug("sequence:>" + sequence + "<");
		String input = sequence;
		
		List<String> output = DetectFormat.split(input);
		for (Iterator iterator = output.iterator(); iterator.hasNext();) {
			String string = (String) iterator.next();
			logger.debug(">" + string.trim() + "<");
		}
		
		Assert.assertEquals(output.size(), 1);

	}

	@Test
	public void splitloop2() {

		String sequence = "RES\n"
				+ "1b:a-dgal-HEX-1:5\n"
				+ "2s:N-acetyl\n"
				+ "3b:b-dgal-HEX-1:5\n"
				+ "4r:r1\n"
				+ "5b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d\n"
				+ "6s:n-acetyl\n"
				+ "7r:r2\n"
				+ "8b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d\n"
				+ "9s:n-acetyl\n"
				+ "LIN\n"
				+ "1:1d(2+1)2n\n"
				+ "2:1o(3+1)3d\n"
				+ "3:3o(3+1)4n\n"
				+ "4:4n(3+2)5d\n"
				+ "5:5d(5+1)6n\n"
				+ "6:1o(6+1)7n\n"
				+ "7:7n(3+2)8d\n"
				+ "8:8d(5+1)9n\n"
				+ "REP\n"
				+ "REP1:13o(3+1)10d=-1--1\n"
				+ "RES\n"
				+ "10b:b-dglc-HEX-1:5\n"
				+ "11s:n-acetyl\n"
				+ "12b:a-lgal-HEX-1:5|6:d\n"
				+ "13b:b-dgal-HEX-1:5\n"
				+ "14s:sulfate\n"
				+ "LIN\n"
				+ "9:10d(2+1)11n\n"
				+ "10:10o(3+1)12d\n"
				+ "11:10o(4+1)13d\n"
				+ "12:10o(6+1)14n\n"
				+ "REP2:18o(3+1)15d=-1--1\n"
				+ "RES\n"
				+ "15b:b-dgal-HEX-1:5\n"
				+ "16s:n-acetyl\n"
				+ "17b:a-lgal-HEX-1:5|6:d\n"
				+ "18b:b-dgal-HEX-1:5\n"
				+ "19s:sulfate\n"
				+ "LIN\n"
				+ "13:15d(2+1)16n\n"
				+ "14:15o(3+1)17d\n"
				+ "15:15o(4+1)18d\n"
				+ "16:15o(6+1)19n";
		
		String sequence3 = "RES\n" + 
	"1b:a-dgal-HEX-1:5\n" + 
				"2s:n-acetyl\n" + 
				"3b:b-dgal-HEX-1:5\n" + 
				"LIN\n" + 
				"1:1d(2+1)2n\n" + 
				"2:1o(3+1)3d";
		
		String sequence2 = "RES\n"
				+ "1b:x-dgal-HEX-1:5\n"
				+ "2s:n-acetyl\n"
				+ "3b:b-dgal-HEX-1:5\n"
				+ "4b:a-dgal-HEX-1:5\n"
				+ "5s:n-acetyl\n"
				+ "6b:b-dgal-HEX-1:5\n"
				+ "7b:b-dglc-HEX-1:5\n"
				+ "8s:n-acetyl\n"
				+ "9s:sulfate\n"
				+ "LIN\n"
				+ "1:1d(2+1)2n\n"
				+ "2:1o(3+1)3d\n"
				+ "3:3o(4+1)4d\n"
				+ "4:4d(2+1)5n\n"
				+ "5:4o(3+1)6d\n"
				+ "6:1o(6+1)7d\n"
				+ "7:7d(2+1)8n\n"
				+ "8:7o(6+1)9n";
		
		String input = sequence + "\n" + sequence2 + "\n" + sequence3;
		logger.debug("sequence:>" + input + "<");
		
		List<String> output = DetectFormat.split(input);
		for (Iterator iterator = output.iterator(); iterator.hasNext();) {
			String string = (String) iterator.next();
			logger.debug(">" + string.trim() + "<");
		}
		Assert.assertEquals(output.size(), 3);
	}	
	
	
	@Test
	public void splitloop3() {
	String sequence = "RES\n" +
	"1b:a-dgal-HEX-1:5\n" +
	"2s:N-acetyl\n" +
	"3b:b-dgal-HEX-1:5\n" +
	"4r:r1\n" +
	"5b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d\n" +
	"6s:n-acetyl\n" +
	"7r:r2\n" +
	"8b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d\n" +
	"9s:n-acetyl\n" +
	"LIN\n" +
	"1:1d(2+1)2n\n" +
	"2:1o(3+1)3d\n" +
	"3:3o(3+1)4n\n" +
	"4:4n(3+2)5d\n" +
	"5:5d(5+1)6n\n" +
	"6:1o(6+1)7n\n" +
	"7:7n(3+2)8d\n" +
	"8:8d(5+1)9n\n" +
	"REP\n" +
	"REP1:13o(3+1)10d=-1--1\n" +
	"RES\n" +
	"10b:b-dglc-HEX-1:5\n" +
	"11s:n-acetyl\n" +
	"12b:a-lgal-HEX-1:5|6:d\n" +
	"13b:b-dgal-HEX-1:5\n" +
	"14s:sulfate\n" +
	"LIN\n" +
	"9:10d(2+1)11n\n" +
	"10:10o(3+1)12d\n" +
	"11:10o(4+1)13d\n" +
	"12:10o(6+1)14n\n" +
	"REP2:18o(3+1)15d=-1--1\n" +
	"RES\n" +
	"15b:b-dgal-HEX-1:5\n" +
	"16s:n-acetyl\n" +
	"17b:a-lgal-HEX-1:5|6:d\n" +
	"18b:b-dgal-HEX-1:5\n" +
	"19s:sulfate\n" +
	"LIN\n" +
	"13:15d(2+1)16n\n" +
	"14:15o(3+1)17d\n" +
	"15:15o(4+1)18d\n" +
	"16:15o(6+1)19n\n" +
	"RES\n" +
	"1r:r1\n" +
	"REP\n" +
	"REP1:8o(4+1)2d=-1--1\n" +
	"RES\n" +
	"2b:b-dgal-HEX-1:5\n" +
	"3s:n-acetyl\n" +
	"4b:b-dglc-HEX-1:5|6:a\n" +
	"5b:b-dgal-HEX-1:5\n" +
	"6s:n-acetyl\n" +
	"7b:a-dgal-HEX-1:5\n" +
	"8b:b-dglc-HEX-1:5\n" +
	"LIN\n" +
	"1:2d(2+1)3n\n" +
	"2:2o(3+1)4d\n" +
	"3:4o(4+1)5d\n" +
	"4:5d(2+1)6n\n" +
	"5:5o(4+1)7d\n" +
	"6:7o(3+1)8d\n";
	logger.debug("sequence:>" + sequence + "<");
	try {
		logger.debug("text1encoded>" + URLEncoder.encode(sequence, "UTF-8") + "<");
	} catch (UnsupportedEncodingException e1) {
		e1.printStackTrace();
	}
	String input = sequence;
	
	List<String> output = DetectFormat.split(input);
	for (Iterator iterator = output.iterator(); iterator.hasNext();) {
		String string = (String) iterator.next();
		logger.debug(">" + string.trim() + "<");
	}
	
	Assert.assertEquals(output.size(), 2);

}


	@Test
	public void splitloop4() {
	String sequence = "RES\r\n" +
	"1b:a-dgal-HEX-1:5\r\n" +
	"2s:N-acetyl\r\n" +
	"3b:b-dgal-HEX-1:5\r\n" +
	"4r:r1\r\n" +
	"5b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d\r\n" +
	"6s:n-acetyl\r\n" +
	"7r:r2\r\n" +
	"8b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d\r\n" +
	"9s:n-acetyl\r\n" +
	"LIN\r\n" +
	"1:1d(2+1)2n\r\n" +
	"2:1o(3+1)3d\r\n" +
	"3:3o(3+1)4n\r\n" +
	"4:4n(3+2)5d\r\n" +
	"5:5d(5+1)6n\r\n" +
	"6:1o(6+1)7n\r\n" +
	"7:7n(3+2)8d\r\n" +
	"8:8d(5+1)9n\r\n" +
	"REP\r\n" +
	"REP1:13o(3+1)10d=-1--1\r\n" +
	"RES\r\n" +
	"10b:b-dglc-HEX-1:5\r\n" +
	"11s:n-acetyl\r\n" +
	"12b:a-lgal-HEX-1:5|6:d\r\n" +
	"13b:b-dgal-HEX-1:5\r\n" +
	"14s:sulfate\r\n" +
	"LIN\r\n" +
	"9:10d(2+1)11n\r\n" +
	"10:10o(3+1)12d\r\n" +
	"11:10o(4+1)13d\r\n" +
	"12:10o(6+1)14n\r\n" +
	"REP2:18o(3+1)15d=-1--1\r\n" +
	"RES\r\n" +
	"15b:b-dgal-HEX-1:5\r\n" +
	"16s:n-acetyl\r\n" +
	"17b:a-lgal-HEX-1:5|6:d\r\n" +
	"18b:b-dgal-HEX-1:5\r\n" +
	"19s:sulfate\r\n" +
	"LIN\r\n" +
	"13:15d(2+1)16n\r\n" +
	"14:15o(3+1)17d\r\n" +
	"15:15o(4+1)18d\r\n" +
	"16:15o(6+1)19n\r\n" +
	"RES\r\n" +
	"1r:r1\r\n" +
	"REP\r\n" +
	"REP1:8o(4+1)2d=-1--1\r\n" +
	"RES\r\n" +
	"2b:b-dgal-HEX-1:5\r\n" +
	"3s:n-acetyl\r\n" +
	"4b:b-dglc-HEX-1:5|6:a\r\n" +
	"5b:b-dgal-HEX-1:5\r\n" +
	"6s:n-acetyl\r\n" +
	"7b:a-dgal-HEX-1:5\r\n" +
	"8b:b-dglc-HEX-1:5\r\n" +
	"LIN\r\n" +
	"1:2d(2+1)3n\r\n" +
	"2:2o(3+1)4d\r\n" +
	"3:4o(4+1)5d\r\n" +
	"4:5d(2+1)6n\r\n" +
	"5:5o(4+1)7d\r\n" +
	"6:7o(3+1)8d\r\n";
	logger.debug("sequence:>" + sequence + "<");
	try {
		logger.debug("text1encoded>" + URLEncoder.encode(sequence, "UTF-8") + "<");
	} catch (UnsupportedEncodingException e1) {
		e1.printStackTrace();
	}
	String input = sequence;
	
	List<String> output = DetectFormat.split(input);
	for (Iterator iterator = output.iterator(); iterator.hasNext();) {
		String string = (String) iterator.next();
		logger.debug(">" + string.trim() + "<");
	}
	Assert.assertEquals(output.size(), 2);

}

	@Test
	public void detectKcf() {
		String kcfOnGlytoucan = "ENTRY     XYZ          Glycan\n"
				+ "NODE      5\n"
				+ "          1     GlcNAc     15.0     7.0\n"
				+ "          2     GlcNAc      8.0     7.0\n"
				+ "          3     Man         1.0     7.0\n"
				+ "          4     Man        -6.0    12.0\n"
				+ "          5     Man        -6.0     2.0\n"
				+ "EDGE      4\n"
				+ "          1     2:b1       1:4\n"
				+ "          2     3:b1       2:4\n"
				+ "          3     5:a1       3:3\n"
				+ "          4     4:a1       3:6\n"
				+ "///\n";
		
		String result = DetectFormat.detect(kcfOnGlytoucan);
		Assert.assertEquals(GlyConvert.KCF, result);
		
	}
	
	
	@Test
	public void detectLinearCode() {
		String kcfOnGlytoucan = "GNb2(Ab4GNb4)Ma3(Ab4GNb2(Fa3(Ab4)GNb6)Ma6)Mb4GNb4GN";
		
		String result = DetectFormat.detect(kcfOnGlytoucan);
		Assert.assertEquals(GlyConvert.LINEARCODE, result);
	}
}