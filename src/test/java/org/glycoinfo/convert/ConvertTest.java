package org.glycoinfo.convert;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glycoinfo.convert.error.ConvertException;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * This is a parent test case class which contains the unit cases found in:
 * https://docs.google.com/spreadsheets/d/
 * 18aTjRJAikC0lBTcIxsOD_9QdozTYoGnbMFx3NisgOl8/edit#gid=0
 * 
 * @author aoki
 *
 */
//@RunWith(SpringJUnit4ClassRunner.class)
public abstract class ConvertTest {

	Log logger = LogFactory.getLog(ConvertTest.class);

	@Autowired
	protected GlyConvert converter;

	HashMap<String, String> conversions = new HashMap<>();

	@Before
	public void setUp() {
		if (null == converter)
			return;
		logger.debug("fromFormat:>" + converter.getFromFormat() + "<");
		logger.debug("toFormat:>" + converter.getToFormat() + "<");
	}

	@Test(expected = ConvertException.class)
	public void testBlank() throws Exception {
		if (null == converter)
			return;
		String actual = converter.convert(null);
		assertEquals("", actual);
		actual = converter.convert("");
		assertEquals("", actual);
	}

	@Test
	public void testG15021LG() throws Exception {
		String num = "G15021LG";
		// the protocol here is the FORMATNAME+TESTCASE#
		conversions.put(GlyConvert.WURCS + num, "WURCS=2.0/1,1,0/[a2122h-1x_1-5]/1/");
		conversions.put(GlyConvert.WURCS_TOPOLOGY + num,
        "WURCS=2.0/2,5,4/[a2122h-1x_1-5_2*NCC/3=O][a1122h-1x_1-5]/1-1-2-2-2/a?-b1_b?-c1_c?-d1_c?-e1");

		conversions.put(GlyConvert.WURCS_SKELETONCODE + num, "a2122h-1x_1-5");
		conversions.put(GlyConvert.GLYCOCT + num, "RES\n" + "1b:x-dglc-HEX-1:5");
		conversions.put(GlyConvert.KCF + num, "ENTRY       G99999                      Glycan\n" + "NODE        1\n"
				+ "            1         Glc   0.0     0.0\n" + "EDGE        0\n" + "///");
		conversions.put(GlyConvert.IUPAC_CONDENSED + num, "Glc");
		conversions.put(GlyConvert.IUPAC + num, "?-D-Glcp(1->");
		conversions.put(GlyConvert.IUPAC_SKELETONCODE + num, "Glc");
		check(num);
	}

	@Test
	public void testG24678II() throws Exception {
		String num = "G24678II";
		conversions.put(GlyConvert.WURCS + num, "WURCS=2.0/1,1,0/[hU122h]/1/");
    conversions.put(GlyConvert.WURCS_TOPOLOGY + num,
        "WURCS=2.0/2,5,4/[a2122h-1x_1-5_2*NCC/3=O][a1122h-1x_1-5]/1-1-2-2-2/a?-b1_b?-c1_c?-d1_c?-e1");
		conversions.put(GlyConvert.WURCS_SKELETONCODE + num, "hU122h");
		conversions.put(GlyConvert.IUPAC_SKELETONCODE + num, "Ara");
		conversions.put(GlyConvert.IUPAC_CONDENSED + num, "Fru");
        conversions.put(GlyConvert.IUPAC_EXTENDED + num, "D-Fru(?->");
		conversions.put(GlyConvert.GLYCOCT + num, "RES\n" + "1b:x-dara-HEX-x:x|2:keto");
		conversions.put(GlyConvert.KCF + num,
				"ENTRY         CT-1             Glycan\n" + "NODE  1\n" + "     1  Fru\n" + "EDGE  0\n" + "///");


		check(num);
	}

	@Test
	public void testG78152OD() throws Exception {
		String num = "G78152OD";
		conversions.put(GlyConvert.WURCS + num, "WURCS=2.0/2,2,1/[ha122h-2b_2-5][a2122h-1a_1-5]/1-2/a2-b1");
    conversions.put(GlyConvert.WURCS_TOPOLOGY + num,
        "WURCS=2.0/2,2,1/[ha122h-2x_2-5][a2122h-1x_1-5]/1-2/a2-b1");
		conversions.put(GlyConvert.WURCS_SKELETONCODE + num, "ha122h-2b_2-5");
		conversions.put(GlyConvert.IUPAC_SKELETONCODE + num, "Fru");
		conversions.put(GlyConvert.IUPAC + num, "alpha-D-Glcp-(1->2)-beta-D-Fruf(2->");
    conversions.put(GlyConvert.IUPAC_CONDENSED + num, "Glca2Frub");
    conversions.put(GlyConvert.IUPAC_EXTENDED + num, "Glca1-2Frub2-R");
		conversions.put(GlyConvert.GLYCOCT + num,
				"RES\n" + "1b:a-dglc-HEX-1:5\n" + "2b:b-dara-HEX-2:5|2:keto\n" + "LIN\n" + "1:1o(1+2)2d");
		conversions.put(GlyConvert.KCF + num,
				"ENTRY         CT-1             Glycan\n" + "		NODE  2\n" + "		     1  Glc   0   0\n"
						+ "		     2  D-Fruf   -8   0\n" + "		EDGE  1\n" + "		     1  2:b2     1:1\n"
						+ "		///");

		check(num);
	}
	
	@Test
	public void testG32272YZ() throws Exception {
		String num = "G32272YZ";
		conversions.put(GlyConvert.WURCS + num, "WURCS=2.0/2,2,1/[ha122h-2b_2-5][a2122h-1a_1-5]/1-2/a2-b1");
		conversions.put(GlyConvert.WURCS_TOPOLOGY + num,
        "WURCS=2.0/2,2,1/[ha122h-2x_2-5][a2122h-1x_1-5]/1-2/a2-b1");
		conversions.put(GlyConvert.WURCS_SKELETONCODE + num, "ha122h-2b_2-5");
		conversions.put(GlyConvert.IUPAC_SKELETONCODE + num, "Fru");
		conversions.put(GlyConvert.IUPAC + num, "alpha-D-Glcp-(1->2)-beta-D-Fruf(2->");
		conversions.put(GlyConvert.IUPAC_CONDENSED + num, "Glca2Frub");
		conversions.put(GlyConvert.IUPAC_EXTENDED + num, "Glca1-2Frub2-R");
		conversions.put(GlyConvert.GLYCOCT + num,
				"RES\n" + "1b:a-dglc-HEX-1:5\n" + "2b:b-dara-HEX-2:5|2:keto\n" + "LIN\n" + "1:1o(1+2)2d");
		conversions.put(GlyConvert.KCF + num,
				"ENTRY         CT-1             Glycan\n" + "		NODE  2\n" + "		     1  Glc   0   0\n"
						+ "		     2  D-Fruf   -8   0\n" + "		EDGE  1\n" + "		     1  2:b2     1:1\n"
						+ "		///");

		check(num);
	}


	@Test
	public void testG38754PY() throws Exception {
		String num = "G38754PY";
		conversions.put(GlyConvert.WURCS + num,
				"WURCS=2.0/2,2,2/[Aad21122h-2a_2-6_5*NCC/3=O][a2112h-1a_1-5]/1-2/a4-b1_a2-b6~n");
    conversions.put(GlyConvert.WURCS_TOPOLOGY + num,
        "WURCS=2.0/2,2,2/[Aad21122h-2x_2-6_5*NCC/3=O][a2112h-1x_1-5]/1-2/a?-b1_a2-b?~n");
		conversions.put(GlyConvert.WURCS_SKELETONCODE + num, "Aad21122h-2a_2-6_5*NCC/3=O");
		conversions.put(GlyConvert.IUPAC_SKELETONCODE + num, "Neu5Ac");
		conversions.put(GlyConvert.IUPAC_CONDENSED + num, "[6Gala4Neu5Aca2]n");
    conversions.put(GlyConvert.IUPAC + num, "[6)-alpha-D-Galp-(1->4)-alpha-D-NeupAc-(2->]");
		conversions.put(GlyConvert.GLYCOCT + num,
				"RES\n" + "1r:r1\n" + "REP\n" + "REP1:3o(6+2)2d=-1--1\n" + "RES\n"
						+ "2b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d\n" + "3b:a-dgal-HEX-1:5\n" + "4s:n-acetyl\n" + "LIN\n"
						+ "1:2o(4+1)3d\n" + "2:2d(5+1)4n");
		conversions.put(GlyConvert.KCF + num, "ERROR! The repeating unit is unsupported in this tool. (GlycoCTtoKCF)");

		check(num);
	}

	@Test
	public void testG00025YC() throws Exception {
		String num = "G00025YC";
		conversions.put(GlyConvert.WURCS + num,
				"WURCS=2.0/6,11,10/[a2122h-1x_1-5_2*NCC/3=O][a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a2112h-1b_1-5][a1221m-1a_1-5]/1-2-3-4-2-5-4-2-6-2-5/a4-b1_a6-i1_b4-c1_c3-d1_c6-g1_d2-e1_e4-f1_g2-h1_j4-k1_j1-d4|d6|g4|g6}");
    conversions.put(GlyConvert.WURCS_TOPOLOGY + num,
        "WURCS=2.0/4,11,10/[a2122h-1x_1-5_2*NCC/3=O][a1122h-1x_1-5][a2112h-1x_1-5][a1221m-1x_1-5]/1-1-2-2-1-3-2-1-4-1-3/a?-b1_a?-i1_b?-c1_c?-d1_c?-g1_d?-e1_e?-f1_g?-h1_j?-k1_d?-g?-j1");
		conversions.put(GlyConvert.WURCS_SKELETONCODE + num, "a2122h-1x_1-5_2*NCC/3=O");
		conversions.put(GlyConvert.IUPAC_SKELETONCODE + num, "GlcNAc");
		conversions.put(GlyConvert.IUPAC_CONDENSED + num, "Galb1-4GlcNAcb1-2Mana1-3(GlcNAcb1-2Mana1-6)Manb1-4GlcNAcb1-4(Fuca1-6)GlcNAc?1-R");
    conversions.put(GlyConvert.IUPAC + num, "[alpha-L-Fucp-(1->6)-[beta-D-GlcpNAc-(1->2)-alpha-D-Manp-(1->6)-beta-D-Galp-(1->4)-beta-D-GlcpNAc-(1->2)-alpha-D-Manp-(1->3)-beta-D-Manp-(1->4)]-beta-D-GlcpNAc-(1->4)-?-D-GlcpNAc(1-]-");
		conversions.put(GlyConvert.GLYCOCT + num,
				"RES\n" + "1b:x-dglc-HEX-1:5\n" + "2s:n-acetyl\n" + "3b:b-dglc-HEX-1:5\n" + "4s:n-acetyl\n"
						+ "5b:b-dman-HEX-1:5\n" + "6b:a-dman-HEX-1:5\n" + "7b:b-dglc-HEX-1:5\n" + "8s:n-acetyl\n"
						+ "9b:b-dgal-HEX-1:5\n" + "10b:a-dman-HEX-1:5\n" + "11b:b-dglc-HEX-1:5\n" + "12s:n-acetyl\n"
						+ "13b:a-lgal-HEX-1:5|6:d\n" + "LIN\n" + "1:1d(2+1)2n\n" + "2:1o(4+1)3d\n" + "3:3d(2+1)4n\n"
						+ "4:3o(4+1)5d\n" + "5:5o(3+1)6d\n" + "6:6o(2+1)7d\n" + "7:7d(2+1)8n\n" + "8:7o(4+1)9d\n"
						+ "9:5o(6+1)10d\n" + "10:10o(2+1)11d\n" + "11:11d(2+1)12n\n" + "12:1o(6+1)13d\n" + "UND\n"
						+ "UND1:100.0:100.0\n" + "ParentIDs:6|10\n" + "SubtreeLinkageID1:o(4|6+1)d\n" + "RES\n"
						+ "14b:b-dglc-HEX-1:5\n" + "15s:n-acetyl\n" + "16b:b-dgal-HEX-1:5\n" + "LIN\n"
						+ "13:14d(2+1)15n\n" + "14:14o(4+1)16d");
		conversions.put(GlyConvert.KCF + num, "ERROR! The undefined unit is unsupported in KCF format. ");

		check(num);
	}

	@Test
	public void testG21074RD() throws Exception {
		String num = "G21074RD";

		conversions.put(GlyConvert.WURCS + num,
				"WURCS=2.0/6,13,12/[a2122h-1x_1-5_2*NCC/3=O][a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a2112h-1b_1-5][a1221m-1a_1-5]/1-2-3-4-2-2-5-4-2-5-2-6-5/a4-b1_b4-c1_c3-d1_c6-h1_d2-e1_d4-f1_f4-g1_h2-i1_h6-k1_i4-j1_k3-l1_k4-m1");
    conversions.put(GlyConvert.WURCS_TOPOLOGY + num,
        "WURCS=2.0/4,13,12/[a2122h-1x_1-5_2*NCC/3=O][a1122h-1x_1-5][a1221m-1x_1-5][a2112h-1x_1-5]/1-1-2-2-1-3-4-1-4-2-1-4-1/a?-b1_b?-c1_c?-d1_c?-j1_d?-e1_d?-h1_e?-f1_e?-g1_h?-i1_j?-k1_j?-m1_k?-l1");
		conversions.put(GlyConvert.WURCS_SKELETONCODE + num, "a2122h-1b_1-5_2*NCC/3=O"); // use 2nd one - beta
		conversions.put(GlyConvert.IUPAC_SKELETONCODE + num, "GlcNAc");
		String expectedGlycoct8 = "RES\n" + "1b:x-dglc-HEX-1:5\n" + "2s:n-acetyl\n" + "3b:b-dglc-HEX-1:5\n"
				+ "4s:n-acetyl\n" + "5b:b-dman-HEX-1:5\n" + "6b:a-dman-HEX-1:5\n" + "7b:b-dglc-HEX-1:5\n"
				+ "8s:n-acetyl\n" + "9b:b-dglc-HEX-1:5\n" + "10s:n-acetyl\n" + "11b:b-dgal-HEX-1:5\n"
				+ "12b:a-dman-HEX-1:5\n" + "13b:b-dglc-HEX-1:5\n" + "14s:n-acetyl\n" + "15b:b-dgal-HEX-1:5\n"
				+ "16b:b-dglc-HEX-1:5\n" + "17s:n-acetyl\n" + "18b:a-lgal-HEX-1:5|6:d\n" + "19b:b-dgal-HEX-1:5\n"
				+ "LIN\n" + "1:1d(2+1)2n\n" + "2:1o(4+1)3d\n" + "3:3d(2+1)4n\n" + "4:3o(4+1)5d\n" + "5:5o(3+1)6d\n"
				+ "6:6o(2+1)7d\n" + "7:7d(2+1)8n\n" + "8:6o(4+1)9d\n" + "9:9d(2+1)10n\n" + "10:9o(4+1)11d\n"
				+ "11:5o(6+1)12d\n" + "12:12o(2+1)13d\n" + "13:13d(2+1)14n\n" + "14:13o(4+1)15d\n" + "15:12o(6+1)16d\n"
				+ "16:16d(2+1)17n\n" + "17:16o(3+1)18d\n" + "18:16o(4+1)19d";
		conversions.put(GlyConvert.IUPAC_CONDENSED + num, "GlcNAcb1-2(Galb1-4GlcNAcb1-4)Mana1-3(Galb1-4GlcNAcb1-2(Fuca1-3(Galb1-4)GlcNAcb1-6)Mana1-6)Manb1-4GlcNAcb1-4GlcNAc?1-R");
    conversions.put(GlyConvert.IUPAC + num, "[beta-D-Galp-(1->4)-[alpha-L-Fucp-(1->3)-beta-D-GlcpNAc-(1->6)]-[beta-D-Galp-(1->4)-beta-D-GlcpNAc-(1->2)-alpha-D-Manp-(1->6)]-[beta-D-Galp-(1->4)-beta-D-GlcpNAc-(1->4)-beta-D-GlcpNAc-(1->2)-alpha-D-Manp-(1->3)]-beta-D-Manp-(1->4)]-beta-D-GlcpNAc-(1->4)-?-D-GlcpNAc(1->");
		conversions.put(GlyConvert.GLYCOCT + num, expectedGlycoct8);
		conversions.put(GlyConvert.LINEARCODE + num, "GNb2(Ab4GNb4)Ma3(Ab4GNb2(Fa3(Ab4)GNb6)Ma6)Mb4GNb4GN");
		conversions.put(GlyConvert.KCF + num,
				"ENTRY         CT-1             Glycan\n" + "NODE  13\n" + "     1  D-GlcpNAc   0   0\n"
						+ "     2  GlcNAc   -8   0\n" + "     3  GlcNAc   -32   6\n" + "     4  GlcNAc   -32   10\n"
						+ "     5  GlcNAc   -32   -12\n" + "     6  GlcNAc   -32   -4\n" + "     7  Man   -24   8\n"
						+ "     8  Gal   -40   10\n" + "     9  Man   -24   -8\n" + "     10  Man   -16   0\n"
						+ "     11  Fuc   -40   -6\n" + "     12  Gal   -40   -12\n" + "     13  Gal   -40   -2\n"
						+ "EDGE  12\n" + "     1  8:b1     4:4\n" + "     2  9:a1     10:6\n" + "     3  5:b1     9:2\n"
						+ "     4  12:b1     5:4\n" + "     5  6:b1     9:6\n" + "     6  11:a1     6:3\n"
						+ "     7  13:b1     6:4\n" + "     8  2:b1     1:4\n" + "     9  10:b1     2:4\n"
						+ "     10  7:a1     10:3\n" + "     11  3:b1     7:2\n" + "     12  4:b1     7:4\n" + "///\n");
		check(num);
	}

	@Test
	public void testG54170UG() throws Exception {
		String num = "G54170UG";

		conversions.put(GlyConvert.GLYCOCT + num,
				"RES\n" + "1r:r1\n" + "REP\n" + "REP1:8o(4+1)2d=-1--1\n" + "RES\n" + "2b:b-dgal-HEX-1:5\n"
						+ "3s:n-acetyl\n" + "4b:b-dglc-HEX-1:5|6:a\n" + "5b:b-dgal-HEX-1:5\n" + "6s:n-acetyl\n"
						+ "7b:a-dgal-HEX-1:5\n" + "8b:b-dglc-HEX-1:5\n" + "LIN\n" + "1:2d(2+1)3n\n" + "2:2o(3+1)4d\n"
						+ "3:4o(4+1)5d\n" + "4:5d(2+1)6n\n" + "5:5o(4+1)7d\n" + "6:7o(3+1)8d");
		conversions.put(GlyConvert.WURCS + num,
				"WURCS=2.0/4,5,5/[a2112h-1b_1-5_2*NCC/3=O][a2122A-1b_1-5][a2112h-1a_1-5][a2122h-1b_1-5]/1-2-1-3-4/a3-b1_b4-c1_c4-d1_d3-e1_a1-e4~n");
		conversions.put(GlyConvert.WURCS_SKELETONCODE + num, "a2112h-1b_1-5_2*NCC/3=O");
    conversions.put(GlyConvert.WURCS_TOPOLOGY + num,
        "WURCS=2.0/4,5,5/[a2112h-1x_1-5_2*NCC/3=O][a2122A-1x_1-5][a2112h-1x_1-5][a2122h-1x_1-5]/1-2-1-3-4/a?-b1_b?-c1_c?-d1_d?-e1_a1-e?~n");
		conversions.put(GlyConvert.IUPAC_SKELETONCODE + num, "GalNAc");
		conversions.put(GlyConvert.KCF + num, "ERROR! The repeating unit is unsupported in this tool. (GlycoCTtoKCF) ");
		conversions.put(GlyConvert.IUPAC + num, "[4)-beta-D-Glcp-(1->3)-alpha-D-Galp-(1->4)-beta-D-GalpNAc-(1->4)-beta-D-GlcpA-(1->3)-beta-D-GalpNAc-(1->]");
		conversions.put(GlyConvert.IUPAC_CONDENSED + num, "[4Glcb3Gala4GalNAcb4GlcAb3GalNAcb1]n");

		check(num);
	}

	@Test
	public void testG20624LQ() throws Exception {
		String num = "G20624LQ";
		conversions.put(GlyConvert.GLYCOCT + num,
				"RES\n" + "1b:x-dglc-HEX-1:5\n" + "2s:n-acetyl\n" + "3b:b-dglc-HEX-1:5\n" + "4s:n-acetyl\n"
						+ "5b:b-dman-HEX-1:5\n" + "6b:a-dman-HEX-1:5\n" + "7b:a-dman-HEX-1:5\n" + "LIN\n"
						+ "1:1d(2+1)2n\n" + "2:1o(4+1)3d\n" + "3:3d(2+1)4n\n" + "4:3o(4+1)5d\n" + "5:5o(3+1)6d\n"
						+ "6:5o(6+1)7d");
		conversions.put(GlyConvert.WURCS + num,
				"WURCS=2.0/4,5,4/[a2122h-1x_1-5_2*NCC/3=O][a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-2-3-4-4/a4-b1_b4-c1_c3-d1_c6-e1");
		
		conversions.put(GlyConvert.WURCS_TOPOLOGY + num,
				"WURCS=2.0/2,5,4/[a2122h-1x_1-5_2*NCC/3=O][a1122h-1x_1-5]/1-1-2-2-2/a?-b1_b?-c1_c?-d1_c?-e1");
		conversions.put(GlyConvert.WURCS_SKELETONCODE + num, "a1122h-1b_1-5");
		conversions.put(GlyConvert.IUPAC_SKELETONCODE + num, "Man");
		conversions.put(GlyConvert.KCF + num,
				"ENTRY     XYZ          Glycan\n" + "NODE      5\n" + "          1     GlcNAc     15.0     7.0\n"
						+ "          2     GlcNAc      8.0     7.0\n" + "          3     Man         1.0     7.0\n"
						+ "          4     Man        -6.0    12.0\n" + "          5     Man        -6.0     2.0\n"
						+ "EDGE      4\n" + "          1     2:b1       1:4\n" + "          2     3:b1       2:4\n"
						+ "          3     5:a1       3:3\n" + "          4     4:a1       3:6\n" + "///");
		
		conversions.put(GlyConvert.IUPAC_CONDENSED + num, "(Mana6Mana3)Manb4GlcNAcb4GlcNAc?-");
		conversions.put(GlyConvert.IUPAC + num, "[alpha-D-Manp-(1->6)-alpha-D-Manp-(1->3)-beta-D-Manp-(1->4)]-beta-D-GlcpNAc-(1->4)-?-D-GlcpNAc(1->");
		check(num);
	}

	@Test
	public void testG29024OJ() throws Exception {
		String num = "G29024OJ";
		conversions.put(GlyConvert.GLYCOCT + num,
				"RES\n" + "1b:x-dglc-HEX-1:5\n" + "2s:n-acetyl\n" + "3b:b-dglc-HEX-1:5\n" + "4s:n-acetyl\n"
						+ "5b:b-dman-HEX-1:5\n" + "6b:a-dman-HEX-1:5\n" + "7b:b-dglc-HEX-1:5\n" + "8s:n-acetyl\n"
						+ "9b:a-dman-HEX-1:5\n" + "10b:b-dglc-HEX-1:5\n" + "11s:n-acetyl\n" + "12b:b-dgal-HEX-1:5\n"
						+ "13b:a-lgal-HEX-1:5|6:d\n" + "LIN\n" + "1:1d(2+1)2n\n" + "2:1o(4+1)3d\n" + "3:3d(2+1)4n\n"
						+ "4:3o(4+1)5d\n" + "5:5o(3+1)6d\n" + "6:6o(2+1)7d\n" + "7:7d(2+1)8n\n" + "8:5o(6+1)9d\n"
						+ "9:9o(2+1)10d\n" + "10:10d(2+1)11n\n" + "11:10o(4+1)12d\n" + "12:1o(6+1)13d");
		conversions.put(GlyConvert.WURCS + num,
				"WURCS=2.0/6,9,8/[a2122h-1x_1-5_2*NCC/3=O][a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a2112h-1b_1-5][a1221m-1a_1-5]/1-2-3-4-2-4-2-5-6/a4-b1_a6-i1_b4-c1_c3-d1_c6-f1_d2-e1_f2-g1_g4-h1");
		conversions.put(GlyConvert.WURCS_SKELETONCODE + num, "a1122h-1a_1-5");
    conversions.put(GlyConvert.WURCS_TOPOLOGY + num,
        "WURCS=2.0/2,5,4/[a2122h-1x_1-5_2*NCC/3=O][a1122h-1x_1-5]/1-1-2-2-2/a?-b1_b?-c1_c?-d1_c?-e1");
		conversions.put(GlyConvert.IUPAC_SKELETONCODE + num, "Man");
		conversions.put(GlyConvert.LINEARCODE + num, "GNb2Ma3(Ab4GNb2Ma6)Mb4GNb4(Fa6)GNa");
		conversions.put(GlyConvert.KCF + num,
				"ENTRY         CT-1             Glycan\n" + "NODE  9\n" + "     1  D-GlcpNAc   0   0\n"
						+ "     2  GlcNAc   -8   4\n" + "     3  GlcNAc   -32   2\n" + "     4  GlcNAc   -32   6\n"
						+ "     5  Man   -24   2\n" + "     6  Fuc   -8   -4\n" + "     7  Man   -24   6\n"
						+ "     8  Man   -16   4\n" + "     9  Gal   -40   6\n" + "EDGE  8\n" + "     1  9:b1     4:4\n"
						+ "     2  6:a1     1:6\n" + "     3  2:b1     1:4\n" + "     4  8:b1     2:4\n"
						+ "     5  5:a1     8:3\n" + "     6  3:b1     5:2\n" + "     7  7:a1     8:6\n"
						+ "     8  4:b1     7:2\n" + "///");
		conversions.put(GlyConvert.IUPAC_CONDENSED + num, "GlcNAcb1-2Mana1-3(Galb1-4GlcNAcb1-2Mana1-6)Manb1-4GlcNAcb1-4(Fuca1-6)GlcNAc?1-R");
    conversions.put(GlyConvert.IUPAC + num, "[alpha-L-Fucp-(1->6)-[beta-D-Galp-(1->4)-beta-D-GlcpNAc-(1->2)-alpha-D-Manp-(1->6)-beta-D-GlcpNAc-(1->2)-alpha-D-Manp-(1->3)-beta-D-Manp-(1->4)]-beta-D-GlcpNAc-(1->4)-?-D-GlcpNAc(1-]-");
		check(num);
		
	}
	
	@Test
	public void testG29504LW() throws Exception {
		String num = "G29504LW";
		conversions.put(GlyConvert.GLYCOCT + num,
				"RES\n"
				+ "1b:x-dgal-HEX-x:x\n"
				+ "2s:n-sulfate\n"
				+ "3b:a-lthr-HEX-1:5|4,5:enx|6:a\n"
				+ "LIN\n"
				+ "1:1d(2+1)2n\n"
				+ "2:1o(3+1)3d");
		conversions.put(GlyConvert.WURCS + num,
				"WURCS=2.0/2,2,1/[u2112h_2*NSO/3=O/3=O][a21EEA-1a_1-5]/1-2/a3-b1");
    conversions.put(GlyConvert.WURCS_TOPOLOGY + num,
        "WURCS=2.0/2,2,1/[u2112h_2*NSO/3=O/3=O][a21EEA-1x_1-5]/1-2/a?-b1");
		conversions.put(GlyConvert.IUPAC + num, "ThrHexAa1-3GalN?-R");
		conversions.put(GlyConvert.IUPAC_CONDENSED + num, "ThrHexAa1-3GalN?-R");
//		Gal(b1-4)GlcNAc(b1-2)[Gal(b1-4)GlcNAc(b1-4)]Man(a1-3)[Fuc(a1-2)Gal(b1-4)GlcNAc(b1-2)Man(a1-6)]Man(b1-4)GlcNAc(b1-4)[Fuc(a1-6)]GlcNAc
		check(num);
	}
	
//	WURCS=2.0/8,11,10/[a2122h-1x_1-5_2*NCC/3=O][a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a2112h-1b_1-5_2*NCC/3=O_4*OSO/3=O/3=O][a2112h-1b_1-5][Aad21122h-2a_2-6_5*NCC/3=O][a1221m-1a_1-5]/1-2-3-4-2-5-4-2-6-7-8/a4-b1_a6-k1_b4-c1_c3-d1_c6-g1_d2-e1_e4-f1_g2-h1_h4-i1_i3-j2
	
  @Test
  public void testG72406BE() throws Exception {
    String num = "G72406BE";
    conversions.put(GlyConvert.GLYCOCT + num,
        "RES\n" + 
        "1b:x-dglc-HEX-1:5\n" + 
        "2s:n-acetyl\n" + 
        "3b:b-dglc-HEX-1:5\n" + 
        "4s:n-acetyl\n" + 
        "5b:b-dman-HEX-1:5\n" + 
        "6b:a-dman-HEX-1:5\n" + 
        "7b:b-dglc-HEX-1:5\n" + 
        "8s:n-acetyl\n" + 
        "9b:b-dgal-HEX-1:5\n" + 
        "10s:n-acetyl\n" + 
        "11s:sulfate\n" + 
        "12b:a-dman-HEX-1:5\n" + 
        "13b:b-dglc-HEX-1:5\n" + 
        "14s:n-acetyl\n" + 
        "15b:b-dgal-HEX-1:5\n" + 
        "16b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d\n" + 
        "17s:n-acetyl\n" + 
        "18b:a-lgal-HEX-1:5|6:d\n" + 
        "LIN\n" + 
        "1:1d(2+1)2n\n" + 
        "2:1o(4+1)3d\n" + 
        "3:3d(2+1)4n\n" + 
        "4:3o(4+1)5d\n" + 
        "5:5o(3+1)6d\n" + 
        "6:6o(2+1)7d\n" + 
        "7:7d(2+1)8n\n" + 
        "8:7o(4+1)9d\n" + 
        "9:9d(2+1)10n\n" + 
        "10:9o(4+1)11n\n" + 
        "11:5o(6+1)12d\n" + 
        "12:12o(2+1)13d\n" + 
        "13:13d(2+1)14n\n" + 
        "14:13o(4+1)15d\n" + 
        "15:15o(3+2)16d\n" + 
        "16:16d(5+1)17n\n" + 
        "17:1o(6+1)18d");
    conversions.put(GlyConvert.WURCS + num,
        "WURCS=2.0/8,11,10/[a2122h-1x_1-5_2*NCC/3=O][a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a2112h-1b_1-5_2*NCC/3=O_4*OSO/3=O/3=O][a2112h-1b_1-5][Aad21122h-2a_2-6_5*NCC/3=O][a1221m-1a_1-5]/1-2-3-4-2-5-4-2-6-7-8/a4-b1_a6-k1_b4-c1_c3-d1_c6-g1_d2-e1_e4-f1_g2-h1_h4-i1_i3-j2");
    conversions.put(GlyConvert.WURCS_TOPOLOGY + num,
        "WURCS=2.0/2,2,2/[h4344h_2*NCC/3=O][a2112h-1x_1-5]/1-2/a?-b1_a?-b?");
    conversions.put(GlyConvert.IUPAC + num, "not supported");
    conversions.put(GlyConvert.IUPAC_CONDENSED + num, "not supported");
    check(num);
  }
  
  public void testG00030VN() throws Exception {
    String num = "G00030VN";
    conversions.put(GlyConvert.GLYCOCT + num,
        "RES\n" + 
        "1b:o-xglc-HEX-0:0|1:aldi\n" + 
        "2s:n-acetyl\n" + 
        "3b:b-dgal-HEX-1:5\n" + 
        "LIN\n" + 
        "1:1d(2+1)2n\n" + 
        "2:1o(4+1)3d\n" + 
        "3:3o(2+6)1d");
    conversions.put(GlyConvert.WURCS + num,
        "WURCS=2.0/2,2,2/[h4344h_2*NCC/3=O][a2112h-1b_1-5]/1-2/a4-b1_a6-b2");
    conversions.put(GlyConvert.WURCS_TOPOLOGY + num,
        "WURCS=2.0/2,2,2/[h4344h_2*NCC/3=O][a2112h-1x_1-5]/1-2/a?-b1_a?-b?");
    conversions.put(GlyConvert.IUPAC + num, "not supported");
    check(num);
  }
	
	private void check(String num) throws ConvertException {
		String input = conversions.get(converter.getFromFormat() + num);
		String expected = conversions.get(converter.getToFormat() + num);
		logger.debug("input:>" + input + "<");
		logger.debug("expected:>" + expected + "<");
		if (null == converter)
			return;
		String actual = converter.convert(input);
		logger.debug("actual:>" + actual + "<");
		assertEquals(expected, actual);
	}
}
