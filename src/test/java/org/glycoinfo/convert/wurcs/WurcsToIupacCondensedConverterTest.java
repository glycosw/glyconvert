/**
 * 
 */
package org.glycoinfo.convert.wurcs;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glycoinfo.convert.ConvertTest;
import org.glycoinfo.convert.GlyConvert;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author aoki
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = WurcsToIupacCondensedConverterTest.class)
@Configuration
public class WurcsToIupacCondensedConverterTest extends ConvertTest {

	Log logger = LogFactory.getLog(WurcsToIupacCondensedConverterTest.class);
	
	@Bean
	public GlyConvert getConverter() {
		return new WurcsToIupacCondensedConverter();
	}
}
