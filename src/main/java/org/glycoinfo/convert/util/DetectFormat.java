package org.glycoinfo.convert.util;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.expasy.sugarconverter.parser.IupacParser;
import org.glycoinfo.convert.GlyConvert;

public class DetectFormat {
	private static Log logger = LogFactory.getLog(DetectFormat.class);

	public static String detect(String sequence) {
		if (null != sequence) {
			if (sequence.trim().startsWith("RES"))
				return GlyConvert.GLYCOCT;
			if (sequence.trim().startsWith("WURCS"))
				return GlyConvert.WURCS;
			if (sequence.trim().startsWith("ENTRY"))
				return GlyConvert.KCF;
			else {
				Pattern pattern = Pattern.compile("([A-Z]{1,3})[ab\\?](\\?|\\d)");
				Matcher matcher = pattern.matcher(sequence);
				int start = 0;
				if (matcher.find(start))
					return GlyConvert.LINEARCODE;
				
			}
			
      IupacParser iupacParser = new IupacParser(sequence);
      try {
        iupacParser.parse();
        return GlyConvert.IUPAC_CONDENSED;
      } catch (Exception e) {
        logger.debug("error when parsing with iupac:>" + e + "<");
        return GlyConvert.INVALID_FORMAT;
      }
		}
		logger.debug("no sequence");
		return GlyConvert.INVALID_FORMAT;
	}
	
	public static List<String> split(String input) {
		input = input.trim();
    logger.debug(input);

    input = input.replaceAll("(?:\r\n|\n)", "\n");
		
		List<String> output = new ArrayList<String>();
		String format = detect(input);
		if (format.equals("glycoct")) {
			// first index
			int i = 0;
			
			input = input.replaceAll("\n", "\t");
			logger.debug(input);
		    Pattern pattern = Pattern.compile("RES\t1[^0-9]");
		    Matcher matcher = pattern.matcher(input);
		    int start = 0;
		    while (matcher.find(start)) {
		        logger.debug("Start index: " + matcher.start());
		        start = matcher.end();
		        if (0 == matcher.start())
		        	continue;
		        logger.debug(" End index: " + matcher.end());
		        logger.debug(" Found: " + matcher.group());
				String substr = input.substring(i, matcher.start());
				substr = substr.replaceAll("\t", "\n");
				output.add(substr);
				i = matcher.start();
		    }
			String substr = input.substring(i, input.length());
			substr = substr.replaceAll("\t", "\n");
			output.add(substr);
		} else {
			StringTokenizer crTok = new StringTokenizer(input, "\r\n");
			while(crTok.hasMoreElements()) {
				output.add(crTok.nextToken());
			}
		}
		return output;
	}

	public static void indexMatch(String text, String regex) {
	    Pattern pattern = Pattern.compile(regex);
	    Matcher matcher = pattern.matcher(text);
	    while (matcher.find()) {
	        logger.debug("Start index: " + matcher.start());
	        logger.debug(" End index: " + matcher.end());
	        logger.debug(" Found: " + matcher.group());
	    }
	}
}
