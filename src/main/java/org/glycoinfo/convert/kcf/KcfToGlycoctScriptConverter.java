package org.glycoinfo.convert.kcf;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

import org.eurocarbdb.MolecularFramework.io.SequenceFormat;
import org.glycoinfo.convert.GlyConvert;
import org.glycoinfo.convert.GlyConvertScript;
import org.glycoinfo.convert.error.ConvertException;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;

/*
 * 
 * <property name="filepass" value="/home/aoki/workspace/GlyConvert/src/main/resources/GlycoctToKcf2.pl" />
 * 
 */
@ConfigurationProperties(prefix="GlycoctToKcfConverter")
@EnableAutoConfiguration
@ComponentScan
public class KcfToGlycoctScriptConverter extends GlyConvertScript {
	List<String> fromFormats = Arrays.asList(SequenceFormat.GLYCOCT_CONDENSED.getName());
	List<String> toFormats = Arrays.asList(SequenceFormat.KCF.getName());

	@Override
	public String convert(String fromSequence) throws ConvertException {
//		setFilepass("");
		String language = "perl"; 
		String[] cmd = {language,getFilepass(),"kcf"};
		String resultStructure = "";
	 try {
	        Runtime runtime = Runtime.getRuntime();
	        Process p = runtime.exec(cmd);
	        InputStream is = p.getInputStream(); 
	        InputStreamReader isr = new InputStreamReader(is);
	        BufferedReader br = new BufferedReader(isr);
	          
			String result;
			while ((result = br.readLine()) != null) {		
				resultStructure = resultStructure + result + "\n";	
			}
	      } catch (IOException ex) {
	        ex.printStackTrace();
	      }
	 return resultStructure;
	}

	@Override
	public String getFromFormat() {
		return GlyConvert.KCF;
	}

	@Override
	public String getToFormat() {
		return GlyConvert.GLYCOCT;
	}
}