package org.glycoinfo.convert.kcf;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glycoinfo.convert.error.ConvertException;
import org.glycoinfo.convert.rings.GlyConvertWebServiceRings;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Primary
public class KcfToGlycoctWSConverter extends GlyConvertWebServiceRings {
	private static final Log logger = LogFactory.getLog(KcfToGlycoctWSConverter.class);
	
	@Override
	public String convert(String fromSequence) throws ConvertException {
		logger.debug("converting from:>" + fromSequence + "<");
		return super.convert(fromSequence);
	}

	@Override
	public String getFromFormat() {
		return RINGS_KCF;
	}

	@Override
	public String getToFormat() {
		return RINGS_GLYCOCT;
	}
}