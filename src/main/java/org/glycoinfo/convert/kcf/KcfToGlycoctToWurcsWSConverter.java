package org.glycoinfo.convert.kcf;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glycoinfo.convert.GlyConvert;
import org.glycoinfo.convert.error.ConvertException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class KcfToGlycoctToWurcsWSConverter extends KcfToGlycoctWSConverter {
	private static final Log logger = LogFactory.getLog(KcfToGlycoctToWurcsWSConverter.class);
	
	@Autowired
	@Qualifier(value = "glycoctToWurcsConverter")
	GlyConvert glycoctToWurcsConverter;
	
	@Override
	public String convert(String fromSequence) throws ConvertException {
		logger.debug("converting from:>" + fromSequence + "<");
		String glycoct = super.convert(fromSequence);
		return glycoctToWurcsConverter.convert(glycoct);
		
	}

	@Override
	public String getFromFormat() {
		return RINGS_KCF;
	}

	@Override
	public String getToFormat() {
		return RINGS_GLYCOCT;
	}
}