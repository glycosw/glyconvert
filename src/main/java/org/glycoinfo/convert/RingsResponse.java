package org.glycoinfo.convert;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RingsResponse {
//	[
//	{"No":0,
//	"status":"true",
//	"submitData":
//		{"format":"Glycoct",
//		 "structure":"RES\\n1b:b-dglc-HEX-1:5\\n2b:b-dgal-HEX-1:5\\n3b:b-dglc-HEX-1:5\\n4s:n-acetyl\\n5b:b-dgal-HEX-1:5\\n6b:b-dglc-HEX-1:5\\n7s:n-acetyl\\n8b:b-dgal-HEX-1:5\\n9b:b-dglc-HEX-1:5\\n10s:n-acetyl\\n11b:a-lgal-HEX-1:5|6:d\\n12b:b-dgal-HEX-1:5\\nLIN\\n1:1o(4+1)2d\\n2:2o(3+1)3d\\n3:3d(2+1)4n\\n4:3o(4+1)5d\\n5:5o(3+1)6d\\n6:6d(2+1)7n\\n7:6o(4+1)8d\\n8:8o(3+1)9d\\n9:9d(2+1)10n\\n10:9o(3+1)11d\\n11:9o(4+1)12d\\n"
//	    },
//	"result":
//	    {"format":"Kcf",
//	     "structure":"ENTRY         CT-1             Glycan\nNODE  0\nEDGE  0\n///\n"
//	    }
//	}
//	]
	
	Integer No;
	String status;
	GlycanSequence submitData;
	GlycanSequence result;
	
	public Integer getNo() {
		return No;
	}
	public void setNo(Integer no) {
		No = no;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public GlycanSequence getSubmitData() {
		return submitData;
	}
	public void setSubmitData(GlycanSequence submitData) {
		this.submitData = submitData;
	}
	public GlycanSequence getResult() {
		return result;
	}
	public void setResult(GlycanSequence result) {
		this.result = result;
	}
	@Override
	public String toString() {
		return "RingsResponse [No=" + No + ", status=" + status + ", submitData=" + submitData + ", result=" + result
				+ "]";
	}
	
	
}
