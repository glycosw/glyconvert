package org.glycoinfo.convert;

import java.util.List;

public abstract class GlyConvertScript implements GlyConvert {
	protected String filepass;

	public String getFilepass() {
		return filepass;
	}

	public void setFilepass(String filepass) {
		this.filepass = filepass;
	}
}
