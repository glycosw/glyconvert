package org.glycoinfo.convert.iupac;

import org.expasy.sugarconverter.parser.IupacParser;
import org.glycoinfo.convert.GlyConvert;
import org.glycoinfo.convert.GlyConvertParent;
import org.glycoinfo.convert.error.ConvertException;
import org.springframework.stereotype.Component;

@Component
public class IupacCondensedToGlycoctConverter extends GlyConvertParent {

	@Override
	public String convert(String fromSequence) throws ConvertException {
		String sequence = check(fromSequence);
    
		IupacParser iupacParser = new IupacParser(sequence);
    //setGlycanType("N-LINKED"); //N-LINKED (-> beta), O-LINKED (-> alpha)
    try
    {
        iupacParser.getCtTree(iupacParser.parse());
        sequence = iupacParser.getCtSequence();
    }
    catch(Exception ex) {
      throw new ConvertException(ex);
    }

		return sequence;
	}

	@Override
	public String getFromFormat() {
		return GlyConvert.IUPAC_CONDENSED;
	}

	@Override
	public String getToFormat() {
		return GlyConvert.GLYCOCT;
	}
}
