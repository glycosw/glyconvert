package org.glycoinfo.convert.iupac;

import org.expasy.sugarconverter.parser.IupacParser;
import org.glycoinfo.convert.GlyConvert;
import org.glycoinfo.convert.GlyConvertParent;
import org.glycoinfo.convert.error.ConvertException;
import org.glycoinfo.convert.glycoct.GlycoctToWurcsConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class IupacCondensedToWurcsConverter extends GlyConvertParent {
  
  @Autowired
  @Qualifier("glycoctToWurcsConverter")
  GlyConvert glycoctToWurcsConverter;
  
	@Override
	public String convert(String fromSequence) throws ConvertException {
		String sequence = check(fromSequence);
    
		IupacParser iupacParser = new IupacParser(sequence);
    try
    {
        iupacParser.getCtTree(iupacParser.parse());
        sequence = iupacParser.getCtSequence();
    }
    catch(Exception ex) {
      throw new ConvertException(ex);
    }
    
		return glycoctToWurcsConverter.convert(sequence);
	}

	@Override
	public String getFromFormat() {
		return GlyConvert.IUPAC_CONDENSED;
	}

	@Override
	public String getToFormat() {
		return GlyConvert.GLYCOCT;
	}
}
