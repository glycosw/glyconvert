package org.glycoinfo.convert.rings;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glycoinfo.convert.GlyConvertWebService;
import org.glycoinfo.convert.GlycanSequence;
import org.glycoinfo.convert.RingsResponse;
import org.glycoinfo.convert.error.ConvertException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

public abstract class GlyConvertWebServiceRings extends GlyConvertWebService {
	
	private static final Log logger = LogFactory.getLog(GlyConvertWebServiceRings.class);

	protected static final String RINGS_KCF = "Kcf";
	protected static final String RINGS_GLYCOCT = "Glycoct";
	protected static final String RINGS_WURCS = "Wurcs";
	protected static final String RINGS_LINEARCODE = "Linearcode";	
	
	@Value("http://rings.t.soka.ac.jp/cgi-bin/tools/utilities/convert/convertJson.pl")
	protected String url;
	
	@Override
	public String convert(String fromSequence) throws ConvertException {
		RestTemplate restTemplate = new RestTemplate();
		RingsResponse[] output = null;
		ResponseEntity<RingsResponse[]> response = null;
		
		String toFormat = getToFormat();
		switch (getToFormat()) {
		case KCF:
			toFormat = RINGS_KCF;
			break;
		case GLYCOCT:
			toFormat = RINGS_GLYCOCT;
		case WURCS:
			toFormat = RINGS_WURCS;
		case LINEARCODE:
			toFormat = RINGS_LINEARCODE;
		default:
			break;
		}
		
		
		String request = url + "?convert_to=" + toFormat + "&in_data=" + fromSequence;
		logger.debug(request);
		// response = restTemplate.getForObject(request,RingsResponse[].class);
		
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
        List<MediaType> supportedMediaTypes = new ArrayList<MediaType>();
        MediaType mediaType = new MediaType("text", "plain", Charset.forName("UTF-8"));
        supportedMediaTypes.add(mediaType);
        MappingJackson2HttpMessageConverter jacksonConverter = new  MappingJackson2HttpMessageConverter();
        jacksonConverter.setSupportedMediaTypes(supportedMediaTypes);
        messageConverters.add(jacksonConverter);
        restTemplate.setMessageConverters(messageConverters);

        try {
        response = restTemplate.getForEntity(request, RingsResponse[].class);
        } catch (HttpMessageNotReadableException e) {
        	logger.warn(e.getMessage());
			return "system error: rings returned a non-parsable JSON:>" + e.getMessage();
        }

        HttpStatus statusCode = response.getStatusCode();
		
		if (null != statusCode && !statusCode.equals(HttpStatus.OK))
			logger.warn("status not OK:>" + statusCode);
		
		output = response.getBody();

		HttpHeaders headers =response.getHeaders(); 
		if (null == output && null != headers && headers.getContentLength() < 1) {
			return "system error: content returned length is 0";
		}
        
		RingsResponse ringsResponse = output[0];
		GlycanSequence sequence = ringsResponse.getResult();
		MediaType contentType = response.getHeaders().getContentType();
		logger.debug(sequence);
		String message = sequence.getMessage();
		
		if (null != message && null != sequence && sequence.getStructure().equals("Error"))
			return message;
		if (output.length < 1)
			return null;
		return sequence.getStructure();
	}
}