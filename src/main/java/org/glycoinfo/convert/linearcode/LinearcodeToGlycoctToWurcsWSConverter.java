package org.glycoinfo.convert.linearcode;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glycoinfo.convert.GlyConvert;
import org.glycoinfo.convert.error.ConvertException;
import org.glycoinfo.convert.rings.GlyConvertWebServiceRings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class LinearcodeToGlycoctToWurcsWSConverter extends GlyConvertWebServiceRings {
	private static final Log logger = LogFactory.getLog(LinearcodeToGlycoctToWurcsWSConverter.class);
	
	@Autowired
	@Qualifier(value = "glycoctToWurcsConverter")
	GlyConvert glycoctToWurcsConverter;	
	
	@Override
	public String convert(String fromSequence) throws ConvertException {
		logger.debug("converting from:>" + fromSequence + "<");
		return glycoctToWurcsConverter.convert(super.convert(fromSequence));
	}

	@Override
	public String getFromFormat() {
		return RINGS_LINEARCODE;
	}

	@Override
	public String getToFormat() {
		return RINGS_GLYCOCT;
	}
}