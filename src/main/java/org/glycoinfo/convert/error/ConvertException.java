package org.glycoinfo.convert.error;

/**
 * 
 * Class for exception of conversion
 * 
 * @author aoki
 * 
 */
public class ConvertException extends Exception {
	protected String msg;

	/**
	 *
	 * @param msg
	 */
	public ConvertException(String msg) {
		super(msg);
		this.msg = msg;
	}
	
	/**
	 * @param e
	 */
	public ConvertException(Throwable e) {
		super(e);
	}

	/**
	 *
	 * @param msg
	 */
	public ConvertException(String msg,Throwable throwable) {
		super(msg,throwable);
		this.msg = msg;
	}

	public String getErrorMessage()
	{
		return this.msg;
	}

	private static final long serialVersionUID = 1L;

}