package org.glycoinfo.convert;

import org.glycoinfo.convert.error.ConvertException;
import org.glycoinfo.convert.error.ConvertFormatException;
import org.glycoinfo.convert.util.DetectFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class GlyConvertDetect {

	@Autowired
	@Qualifier(value = "glycoctToWurcsConverter")
	GlyConvert glycoctToWurcsConverter;

	@Autowired
	@Qualifier(value = "kcfToWurcsConverter")
	GlyConvert kcfToWurcsConverter;

	@Autowired
	@Qualifier("linearcodeToWurcsConverter")
	GlyConvert linearcodeToWurcsConverter;

	@Autowired
	@Qualifier("iupacCondensedToWurcsConverter")
	GlyConvert iupacCondensedToWurcsConverter;
	
	public static String CannotDetectFormat = "CannotDetectFormat";

	public String convert(String fromSequence, String toFormat) throws ConvertException {
		String fromformat = DetectFormat.detect(fromSequence);
		
		GlyConvert converter = null;

		if (toFormat.equals(GlyConvert.WURCS) && fromformat.equals(GlyConvert.GLYCOCT)) {
			converter = glycoctToWurcsConverter;
		} else if (toFormat.equals(GlyConvert.WURCS) && fromformat.equals(GlyConvert.KCF)) {
			converter = kcfToWurcsConverter;
		} else if (toFormat.equals(GlyConvert.WURCS) && fromformat.equals(GlyConvert.LINEARCODE)) {
			converter = linearcodeToWurcsConverter;
		} else if (toFormat.equals(GlyConvert.WURCS) && fromformat.equals(GlyConvert.IUPAC_CONDENSED)) {
			converter = iupacCondensedToWurcsConverter;
		} else if (toFormat.equals(fromformat)) {
			return fromSequence;
		}
		else
			throw new ConvertFormatException(CannotDetectFormat);
		
		return converter.convert(fromSequence);
	}
}