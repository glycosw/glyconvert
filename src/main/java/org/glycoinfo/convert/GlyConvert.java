package org.glycoinfo.convert;

import org.glycoinfo.convert.error.ConvertException;


/**
 * @author aoki
 *
 */
public interface GlyConvert {

	public static final String WURCS = "wurcs";
  public static final String WURCS_COMPOSITION = "wurcs_composition";
  public static final String WURCS_BASECOMPOSITION = "wurcs_basecomposition";
	public static final String WURCS_TOPOLOGY = "wurcs_topology";
	public static final String KCF = "kcf";
	public static final String GLYCOCT = "glycoct";
	public static final String INVALID_FORMAT = "invalid";
	public static final String LINEARCODE = "linearcode";
	public static final String CARBBANK = "carbbank";
	public static final String IUPAC_EXTENDED = "iupac_extended";
	public static final String IUPAC = IUPAC_EXTENDED;
	public static final String IUPAC_CONDENSED = "iupac_condensed";
	public static final String WURCS_SKELETONCODE = "WurcsSkeletonCode";
	public static final String IUPAC_SKELETONCODE = "IupacSkeletonCode";


	/**
	 * @return String result of conversion.
	 * @throws Exception
	 */
	public String convert(String fromSequence) throws ConvertException;

	public String getFromFormat();

	public String getToFormat();
}