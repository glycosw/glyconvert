package org.glycoinfo.convert;

import org.glycoinfo.convert.glycoct.GlycoctToKcfWSConverter;
import org.glycoinfo.convert.glycoct.GlycoctToWurcsConverter;
import org.glycoinfo.convert.iupac.IupacCondensedToGlycoctConverter;
import org.glycoinfo.convert.iupac.IupacCondensedToWurcsConverter;
import org.glycoinfo.convert.kcf.KcfToGlycoctToWurcsWSConverter;
import org.glycoinfo.convert.kcf.KcfToGlycoctWSConverter;
import org.glycoinfo.convert.kcf.KcfToWurcsWSConverter;
import org.glycoinfo.convert.linearcode.LinearcodeToGlycoctToWurcsWSConverter;
import org.glycoinfo.convert.linearcode.LinearcodeToGlycoctWSConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GlyConvertConfig {

	@Bean(name = "kcfToGlycoCTConverter")
	GlyConvert kcfToGlycoCTConverter() {
		return new KcfToGlycoctWSConverter();
	}
	
	@Bean(name = "kcfToGlycoCTToWurcsConverter")
	GlyConvert kcfToGlycoCTToWurcsConverter() {
		return new KcfToGlycoctToWurcsWSConverter();
	}
	
	@Bean(name = "kcfToWurcsConverter")
	GlyConvert kcfToWurcsConverter() {
		return new KcfToWurcsWSConverter();
	}
	
	@Bean(name = "glycoctToWurcsConverter")
	GlyConvert glycoctToWurcsConverter() {
		return new GlycoctToWurcsConverter();
	}
	
	@Bean(name = "glycoctToKcfConverter")
	GlyConvert glycoctToKcfConverter() {
		return new GlycoctToKcfWSConverter();
	}

	@Bean(name = "linearcodeToGlycoCTConverter")
	GlyConvert linearcodeToGlycoCTConverter() {
		return new LinearcodeToGlycoctWSConverter();
	}
	
	@Bean(name = "linearcodeToWurcsConverter")
	GlyConvert linearcodeToWurcsConverter() {
		return new LinearcodeToGlycoctToWurcsWSConverter();
	}
	
  @Bean(name = "wurcsToWurcstopologyConverter")
  GlyConvert wurcsToWurcstopologyConverter() {
    return new LinearcodeToGlycoctToWurcsWSConverter();
  }
  
  @Bean(name = "iupacCondensedToGlycoctConverter")
  GlyConvert iupacCondensedToGlycoctConverter() {
    return new IupacCondensedToGlycoctConverter();
  }
  
  @Bean(name = "iupacCondensedToWurcsConverter")
  GlyConvert iupacCondensedToWurcsConverter() {
    return new IupacCondensedToWurcsConverter();
  }
	
	@Bean
	GlyConvertDetect glyConvertDetect() {
		return new GlyConvertDetect();
	}
}
