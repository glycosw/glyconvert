package org.glycoinfo.convert;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RingsRequest {

	String datasetname;
	String convert_path;
	String convert_to;
	String type;
	
	public String getDatasetname() {
		return datasetname;
	}
	public void setDatasetname(String datasetname) {
		this.datasetname = datasetname;
	}
	public String getConvert_path() {
		return convert_path;
	}
	public void setConvert_path(String convert_path) {
		this.convert_path = convert_path;
	}
	public String getConvert_to() {
		return convert_to;
	}
	public void setConvert_to(String convert_to) {
		this.convert_to = convert_to;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	
	
}
