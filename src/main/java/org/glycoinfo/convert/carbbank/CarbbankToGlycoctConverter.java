package org.glycoinfo.convert.carbbank;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eurocarbdb.MolecularFramework.io.SugarImporter;
import org.eurocarbdb.MolecularFramework.io.SugarImporterException;
import org.eurocarbdb.MolecularFramework.io.GlycoCT.SugarExporterGlycoCTCondensed;
import org.eurocarbdb.MolecularFramework.io.carbbank.SugarImporterCarbbank;
import org.eurocarbdb.MolecularFramework.io.namespace.GlycoVisitorToGlycoCT;
import org.eurocarbdb.MolecularFramework.sugar.Sugar;
import org.eurocarbdb.MolecularFramework.util.visitor.GlycoVisitorException;
import org.eurocarbdb.resourcesdb.Config;
import org.eurocarbdb.resourcesdb.io.MonosaccharideConverter;
import org.glycoinfo.convert.GlyConvert;
import org.glycoinfo.convert.error.ConvertException;
import org.springframework.stereotype.Component;

@Component
public class CarbbankToGlycoctConverter implements GlyConvert {

	protected Log logger = LogFactory.getLog(getClass());
	String from;
	
	@Override
	public String convert(String fromSequence) throws ConvertException {
		SugarImporter t_objImporter = new SugarImporterCarbbank();
		Config t_objConf = new Config();

		MonosaccharideConverter t_objTrans = new MonosaccharideConverter(t_objConf);
		Sugar g1;
		try {
			g1 = t_objImporter.parse(fromSequence);
		} catch (SugarImporterException e) {
			throw new ConvertException(e);
		}

		SugarExporterGlycoCTCondensed exp = new SugarExporterGlycoCTCondensed ();
		GlycoVisitorToGlycoCT t_objTo = new GlycoVisitorToGlycoCT(t_objTrans);
		try {
			t_objTo.start(g1);
		g1 = t_objTo.getNormalizedSugar();

		exp.start(g1);
		} catch (GlycoVisitorException e) {
			throw new ConvertException(e);
		}

		return exp.getHashCode();
	}

	@Override
	public String getFromFormat() {
		return GlyConvert.CARBBANK;
	}

	@Override
	public String getToFormat() {
		return GlyConvert.GLYCOCT;
	}
}