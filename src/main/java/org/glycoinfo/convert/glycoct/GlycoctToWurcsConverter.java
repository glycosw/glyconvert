package org.glycoinfo.convert.glycoct;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eurocarbdb.MolecularFramework.io.SugarImporter;
import org.eurocarbdb.MolecularFramework.io.SugarImporterException;
import org.eurocarbdb.MolecularFramework.io.GlycoCT.SugarImporterGlycoCTCondensed;
import org.eurocarbdb.MolecularFramework.io.namespace.GlycoVisitorToGlycoCT;
import org.eurocarbdb.MolecularFramework.sugar.GlycoconjugateException;
import org.eurocarbdb.MolecularFramework.sugar.Sugar;
import org.eurocarbdb.MolecularFramework.util.validation.GlycoVisitorValidation;
import org.eurocarbdb.MolecularFramework.util.visitor.GlycoVisitorException;
import org.eurocarbdb.resourcesdb.Config;
import org.eurocarbdb.resourcesdb.io.MonosaccharideConverter;
import org.glycoinfo.WURCSFramework.util.GlycoVisitorValidationForWURCS;
import org.glycoinfo.WURCSFramework.util.WURCSException;
import org.glycoinfo.WURCSFramework.util.WURCSFactory;
import org.glycoinfo.WURCSFramework.util.exchange.SugarToWURCSGraph;
import org.glycoinfo.WURCSFramework.wurcs.graph.WURCSGraph;
import org.glycoinfo.convert.GlyConvert;
import org.glycoinfo.convert.GlyConvertParent;
import org.glycoinfo.convert.error.ConvertException;
import org.springframework.stereotype.Component;

@Component
public class GlycoctToWurcsConverter extends GlyConvertParent {

	protected Log logger = LogFactory.getLog(getClass());
	String from;
	
	@Override
	public String convert(String fromSequence) throws ConvertException {
		String t_strCode = check(fromSequence);
		if (t_strCode.contains("\\n")) {
			logger.warn("contained \\\\n, replacing with \\n");
			t_strCode = t_strCode.replaceAll("(?:\\\\n)", "\n");
		}
		if (!t_strCode.endsWith("\n")) {
			logger.warn("t_strCode:>"+t_strCode+"does not end with \\n, adding");
			t_strCode = t_strCode + "\n";
		}

		logger.debug("converting:>" + t_strCode + "<");
		SugarImporter t_objImporterGlycoCT = new SugarImporterGlycoCTCondensed();
		SugarToWURCSGraph t_objExporterWURCSGraph = new SugarToWURCSGraph();
		
		StringBuffer t_strLog = new StringBuffer("");
		String t_strWURCS = null;
		try {
			Sugar t_oSugar = t_objImporterGlycoCT.parse(t_strCode);
			Sugar validate = t_oSugar.copy();
			t_oSugar = normalizeSugar(validate, t_strLog);
			
			t_objExporterWURCSGraph.start(t_oSugar);

			SugarToWURCSGraph t_oS2G = new SugarToWURCSGraph();
			t_oS2G.start(t_oSugar);
			WURCSGraph t_oGraph = t_oS2G.getGraph();

			// Get WURCS string
			WURCSFactory t_oFactory = new WURCSFactory(t_oGraph);
			t_strWURCS = t_oFactory.getWURCS();
			System.out.println(t_strWURCS);

		} catch ( SugarImporterException | GlycoVisitorException | WURCSException | GlycoconjugateException e ) {
			throw new ConvertException(e.getMessage(), e);
		} catch ( StringIndexOutOfBoundsException e ) {
			logger.warn("Possible format error in glycoct");
			throw new ConvertException(e);
		}
//		SugarImporter t_objImporter = new SugarImporterGlycoCTCondensed();
//		String t_strCode = getFromSequence();
//		MonosaccharideConverter t_objTrans = new MonosaccharideConverter(
//				new Config());
//		Sugar g1 = null;
//		try {
//			g1 = t_objImporter.parse(t_strCode);
//		} catch (SugarImporterException e1) {
//			e1.printStackTrace();
//			throw new ConvertException(e1);
//		}
//		GlycoVisitorToGlycoCT t_objTo = new GlycoVisitorToGlycoCT(t_objTrans);
//		String result = null;
//		try {
//			t_objTo.start(g1);
//			g1 = t_objTo.getNormalizedSugar();
//			SugarExporterGlycoCTCondensed t_exporter2 = new SugarExporterGlycoCTCondensed();
//			t_exporter2.start(g1);
//			logger.debug("t_exporter2.getHashCode():>" + t_exporter2.getHashCode() + "<");
//
//			SugarExporterWURC t_exporter3 = new SugarExporterWURCS();
//			try {
//				t_exporter3.start(g1);
//			} catch (GlycoVisitorException e) {
//				logger.error(e.getErrorMessage());
//			}
//			result = t_exporter3.getWURCSCompress();
//			logger.debug(result);
//
//		} catch (GlycoVisitorException e1) {
//			e1.printStackTrace();
//			throw new ConvertException(e1);
//		}
		return t_strWURCS;
	}
	
	/** Nomarize and validate sugar */
	public Sugar normalizeSugar(Sugar a_objSugar, StringBuffer a_strLog) throws GlycoVisitorException {

		// Validate sugar
		GlycoVisitorValidation t_validation = new GlycoVisitorValidation();
		t_validation.start(a_objSugar);
		List<String> t_aErrorStrings   = t_validation.getErrors();
		List<String> t_aWarningStrings = t_validation.getWarnings();

		// Remove error "Sugar has more than one root residue." for composition
		while( t_aErrorStrings.contains("Sugar has more than one root residue.") )
			t_aErrorStrings.remove( t_aErrorStrings.indexOf("Sugar has more than one root residue.") );
		Sugar copy = null;
		try {
			copy = a_objSugar.copy();
		} catch (GlycoconjugateException e) {
			throw new GlycoVisitorException(e.getMessage());
		}
		// Validate for WURCS
		GlycoVisitorValidationForWURCS t_validationWURCS = new GlycoVisitorValidationForWURCS();
		t_validationWURCS.start(copy);

		// Marge errors and warnings
		t_aErrorStrings.addAll( t_validationWURCS.getErrors() );
		t_aWarningStrings.addAll( t_validationWURCS.getWarnings() );
		if ( !t_aErrorStrings.isEmpty() )
			a_strLog.append("Errors:\n");
		for ( String err : t_aErrorStrings )
			a_strLog.append(err+"\n");

		if ( !t_aWarningStrings.isEmpty() )
			a_strLog.append("Warnings:\n");
		for ( String warn : t_aWarningStrings ) {
			a_strLog.append(warn+"\n");
		}
		if ( !t_aErrorStrings.isEmpty() ) {
			String msg = "";
			for (String string : t_aErrorStrings) {
				msg += string;
			}
			
			throw new GlycoVisitorException("Error in GlycoCT validation:>" + msg + "<");
		}

		// Normalize sugar
		GlycoVisitorToGlycoCT t_objTo
			= new GlycoVisitorToGlycoCT( new MonosaccharideConverter( new Config() ) );
		t_objTo.start(a_objSugar);
		return t_objTo.getNormalizedSugar();
	}

	@Override
	public String getFromFormat() {
		return GlyConvert.GLYCOCT;
	}

	@Override
	public String getToFormat() {
		return GlyConvert.WURCS;
	}
}