package org.glycoinfo.convert.glycoct;

import java.io.IOException;

import org.eurocarbdb.MolecularFramework.io.SugarExporterException;
import org.eurocarbdb.MolecularFramework.io.SugarImporter;
import org.eurocarbdb.MolecularFramework.io.SugarImporterException;
import org.eurocarbdb.MolecularFramework.io.GlycoCT.SugarImporterGlycoCTCondensed;
import org.eurocarbdb.MolecularFramework.io.kcf.SugarExporterKcf;
import org.eurocarbdb.MolecularFramework.io.namespace.GlycoVisitorFromGlycoCT;
import org.eurocarbdb.MolecularFramework.sugar.Sugar;
import org.eurocarbdb.MolecularFramework.util.visitor.GlycoVisitorException;
import org.eurocarbdb.resourcesdb.GlycanNamescheme;
import org.eurocarbdb.resourcesdb.io.MonosaccharideConversion;
import org.glycoinfo.convert.GlyConvert;
import org.glycoinfo.convert.GlyConvertParent;
import org.glycoinfo.convert.error.ConvertException;
import org.glycomedb.residuetranslator.ResidueTranslator;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;

/*
 * 
 * <property name="filepass" value="/home/aoki/workspace/GlyConvert/src/main/resources/GlycoctToKcf2.pl" />
 * 
 */
@ConfigurationProperties(prefix = "GlycoctToKcfConverter")
@EnableAutoConfiguration
@ComponentScan
public class GlycoctToLinearcodeMFConverter extends GlyConvertParent {

	@Override
	public String convert(String fromSequence) throws ConvertException {
		SugarImporter t_objImporter = new SugarImporterGlycoCTCondensed();
		// Config t_objConf = new Config();

		// MonosaccharideConversion t_objTrans = new
		// MonosaccharideConverter(t_objConf);
		MonosaccharideConversion t_translator;
		try {
			t_translator = new ResidueTranslator();
		} catch (IOException e) {
			throw new ConvertException(e);
		}
		Sugar g1;
		try {
			g1 = t_objImporter.parse(fromSequence);
		} catch (SugarImporterException e) {
			throw new ConvertException(e);
		}

		// GlycoVisitorFromGlycoCT t_objTo = new
		// GlycoVisitorFromGlycoCT(t_objTrans);
		GlycoVisitorFromGlycoCT t_objTo = new GlycoVisitorFromGlycoCT(t_translator, GlycanNamescheme.KEGG);

		try {
			t_objTo.start(g1);
		} catch (GlycoVisitorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		g1 = t_objTo.getNormalizedSugar();

		SugarExporterKcf exp = new SugarExporterKcf();
		String result = null;
		try {
			result = exp.export(g1);
		} catch (SugarExporterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public String getFromFormat() {
		return GlyConvert.GLYCOCT;
	}

	@Override
	public String getToFormat() {
		return GlyConvert.KCF;
	}
}