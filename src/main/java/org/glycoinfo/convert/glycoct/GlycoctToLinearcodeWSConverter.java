package org.glycoinfo.convert.glycoct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glycoinfo.convert.rings.GlyConvertWebServiceRings;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

/**
 * 
 * utilizes the RINGS web service such as: 
 * ?convert_to=Kcf&in_data=RES%5Cn1b%3Ab-dglc-HEX-1%3A5%5Cn2b%3Ab-dgal-HEX-1%3A5%5Cn3b%3Ab-dglc-HEX-1%3A5%5Cn4s%3An-acetyl%5Cn5b%3Ab-dgal-HEX-1%3A5%5Cn6b%3Ab-dglc-HEX-1%3A5%5Cn7s%3An-acetyl%5Cn8b%3Ab-dgal-HEX-1%3A5%5Cn9b%3Ab-dglc-HEX-1%3A5%5Cn10s%3An-acetyl%5Cn11b%3Aa-lgal-HEX-1%3A5%7C6%3Ad%5Cn12b%3Ab-dgal-HEX-1%3A5%5CnLIN%5Cn1%3A1o%284%2B1%292d%5Cn2%3A2o%283%2B1%293d%5Cn3%3A3d%282%2B1%294n%5Cn4%3A3o%284%2B1%295d%5Cn5%3A5o%283%2B1%296d%5Cn6%3A6d%282%2B1%297n%5Cn7%3A6o%284%2B1%298d%5Cn8%3A8o%283%2B1%299d%5Cn9%3A9d%282%2B1%2910n%5Cn10%3A9o%283%2B1%2911d%5Cn11%3A9o%284%2B1%2912d%5Cn
 *
 * to convert from glycoct to KCF.
 * 
 * @author aoki
 *
 * This work is licensed under the Creative Commons Attribution 4.0 International License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.
 *
 */
@Component
@Primary
public class GlycoctToLinearcodeWSConverter extends GlyConvertWebServiceRings {
	private static final Log logger = LogFactory.getLog(GlycoctToLinearcodeWSConverter.class);
	
	@Override
	public String getFromFormat() {
		return GLYCOCT;
	}

	@Override
	public String getToFormat() {
		return LINEARCODE;
	}
}