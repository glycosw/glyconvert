package org.glycoinfo.convert.wurcs;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glycoinfo.WURCSFramework.util.graph.analysis.SubsumptionLevel;
import org.glycoinfo.convert.GlyConvertParent;
import org.glycoinfo.convert.error.ConvertException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.glycoinfo.subsumption.SubsumptionConverter;
import org.glycoinfo.subsumption.SubsumptionException;
import org.glycoinfo.subsumption.Subsumption_Converter;

@Component
public class WurcsToWurcsCompositionConverter extends GlyConvertParent {
  private static final Log logger = LogFactory.getLog(WurcsToWurcsCompositionConverter.class);

	public String convert(String fromSequence) throws ConvertException {
		
		// Subsumption_Converterを呼び出す
		String str_WurcsComposition= check(fromSequence);
		SubsumptionConverter a_converter = new SubsumptionConverter();
		
			a_converter.setWURCSseq(fromSequence);
			
			
			try {
        a_converter.convertDefined2Ambiguous();
      } catch (SubsumptionException sub) {
        logger.debug("SubsumptionException:>" + sub);
        logger.debug("SubsumptionException:>" + sub.getMessage());
        throw new ConvertException(sub.getErrorMessage(), sub);
      }	catch (Exception e) {
        throw new ConvertException(e);
      }
			
      if (a_converter.getWURCSlevel() != SubsumptionLevel.LV2.getLevel()) {
        throw new ConvertException("Input WURCS was not level 2");
      }
			if (a_converter.getAmbiguousWURCSlevel() != SubsumptionLevel.LV3.getLevel()) {
			  logger.error("Composition did not become level 3");
			  if (a_converter.getAmbiguousWURCSlevel() == SubsumptionLevel.LV4.getLevel())
			    return null;
			  throw new ConvertException("Composition did not become level 3 nor 4");
			  
			}
			
			str_WurcsComposition = a_converter.getAmbiguousWURCSseq();
		
		return str_WurcsComposition;
	}

	@Override
	public String getFromFormat() {
		return WURCS;
	}

	@Override
	public String getToFormat() {
		return WURCS_COMPOSITION;
	}

}
