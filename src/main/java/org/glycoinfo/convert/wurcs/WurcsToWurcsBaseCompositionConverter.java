package org.glycoinfo.convert.wurcs;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glycoinfo.WURCSFramework.util.graph.analysis.SubsumptionLevel;
import org.glycoinfo.convert.GlyConvertParent;
import org.glycoinfo.convert.error.ConvertException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.glycoinfo.subsumption.SubsumptionConverter;
import org.glycoinfo.subsumption.SubsumptionException;
import org.glycoinfo.subsumption.Subsumption_Converter;

@Component
public class WurcsToWurcsBaseCompositionConverter extends GlyConvertParent {
  private static final Log logger = LogFactory.getLog(WurcsToWurcsBaseCompositionConverter.class);

	public String convert(String fromSequence) throws ConvertException {
		
		// Subsumption_Converterを呼び出す
		String str_WurcsBaseComposition= check(fromSequence);
		SubsumptionConverter a_converter = new SubsumptionConverter();
		
			a_converter.setWURCSseq(fromSequence);
			
			try {
        a_converter.convertDefined2Ambiguous();
      } catch (SubsumptionException sub) {
        logger.debug("SubsumptionException:>" + sub);
        logger.debug("SubsumptionException:>" + sub.getMessage());
        throw new ConvertException(sub.getErrorMessage(), sub);
      }	catch (Exception e) {
        throw new ConvertException(e);
      }
			
      if (a_converter.getWURCSlevel() != SubsumptionLevel.LV3.getLevel()) {
        throw new ConvertException("Input WURCS was not level 3");
      }
			if (a_converter.getAmbiguousWURCSlevel() != SubsumptionLevel.LV4.getLevel()) {
			  throw new ConvertException("BaseComposition did not become level 4");
			}
			
			str_WurcsBaseComposition = a_converter.getAmbiguousWURCSseq();
		
		return str_WurcsBaseComposition;
	}

	@Override
	public String getFromFormat() {
		return WURCS;
	}

	@Override
	public String getToFormat() {
		return WURCS_BASECOMPOSITION;
	}

}
