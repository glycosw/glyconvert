package org.glycoinfo.convert.wurcs;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glycoinfo.convert.GlyConvertParent;
import org.glycoinfo.convert.error.ConvertException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.glycoinfo.subsumption.SubsumptionException;
import org.glycoinfo.subsumption.Subsumption_Converter;

@Component
public class WurcsToWurcsTopologyConverter extends GlyConvertParent {
  private static final Log logger = LogFactory.getLog(WurcsToWurcsTopologyConverter.class);

	public String convert(String fromSequence) throws ConvertException {
		
		// Subsumption_Converterを呼び出す
		String str_WurcsToplogy= check(fromSequence);
		Subsumption_Converter a_converter = new Subsumption_Converter();
		
			a_converter.setWURCSseq(fromSequence);
			try {
        a_converter.convertLinkageInfo2Topology();
      } catch (SubsumptionException sub) {
        logger.debug("SubsumptionException:>" + sub);
        logger.debug("SubsumptionException:>" + sub.getMessage());
        throw new ConvertException(sub.getErrorMessage(), sub);
      }	catch (Exception e) {
        throw new ConvertException(e);
      }
			str_WurcsToplogy = a_converter.getWURCStopology();
		
		return str_WurcsToplogy;
	}

	@Override
	public String getFromFormat() {
		return WURCS;
	}

	@Override
	public String getToFormat() {
		return WURCS_TOPOLOGY;
	}

}
