# Moved
[Moved to Gitlab](https://gitlab.com/glycosmos/)

under glyconvert project.

# Meta-Converter of Glycan Structures.

This library incorporates various other conversion methods (libraries, web services, etc) in order to convert a string input into a converted string as output.

## What is this for?

Anyone who needs to convert from one string to another in Java.  As it utilizes the best known methods to convert, it can be a singular library to convert all methods.

[Test Cases and Conversion status](https://docs.google.com/spreadsheets/d/18aTjRJAikC0lBTcIxsOD_9QdozTYoGnbMFx3NisgOl8/edit#gid=0)

### How do I get set up? ###

* mvn package

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* @aokinobu 
